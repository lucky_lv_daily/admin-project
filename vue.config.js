const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: true, // 关闭eslint校验
  /* webpack-dev-server 相关配置 */
  devServer: {
    proxy: {
      '/URLA': {
        target: 'http://tlight-information-mgr.jufxt.com', // 测试环境
        changeOrigin: true,
        pathRewrite: { '^/URLA': '' }
      }
    }
  },
})
