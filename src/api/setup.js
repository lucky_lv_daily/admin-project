import api from '@/utils/request'
const prefix = 'setup_';
const prefixUrl = '/sys-service';

export default {
    [prefix + '001']: (params) =>
        api.A.get({
            url: `${prefixUrl}/sysdept`,
            params
        }),
}
