import api from '@/utils/request'
import setup from './setup.js';

export default {
    ...setup,
    _000: (params) =>
        api.A.post({
            url: "/sys_user/add",
            params,
        }),
    _001: (params) =>
        api.A.get({
            url: '/sys_user/page',
            params
        }),

    _023: (params) =>
        api.A.postJson({
            url: "/article/save",
            params,
        }),
    _005: (params) =>
        api.A.postJson({
            url: "/sys_user/login",
            params,
        }),
}
