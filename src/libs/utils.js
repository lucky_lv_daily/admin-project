/**
 * @author: LH
 * @description: 创建地址路径
 * @param {*} name
 */
const makePath = function (list, root) {
    function minxPath(name) {
        let path = root || '';
        for (let i = 0; i < name.length; i++) {
            path += name[i].charCodeAt().toString(36);
        }
        return path;
        // return path + new Date().getDate();
    }
    list.forEach((item) => {
        if (item.path === undefined) {
            item.path = minxPath(item.name);
        }
        // console.log(item.path);
        // console.log(item.path);
    });
    return list;
};

export default {
    makePath
}
