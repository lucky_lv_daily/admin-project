const currentAppEvn = process.env.VUE_APP_EVN || 'test'; // 当前环境
// 环境变量集
const evnList = {
    // 本地环境
    dev: {
        id: 'dev',
        baseUrlA: 'URLA', // axios接口请求A地址
    },
    // 测试环境
    test: {
        id: 'test',
        baseUrlA: 'http://tlight-information-mgr.jufxt.com', // axios接口请求A地址
    },
    // 生产环境
    production: {
        id: 'production',
        baseUrlA: 'https://adapi-admin.ihaogu.com', // axios接口请求A地址
    }
};

export default evnList[currentAppEvn];
