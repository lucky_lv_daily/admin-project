class Storage {
    constructor(type) {
        this.systemKey = 'admin'; // 系统级别标识
        this.type = type ? type + 'Storage' : 'sessionStorage';
    }

    prefix(str) {
        const prefix = this.systemKey + str;
        return String(prefix).toUpperCase().split('').reverse().join('I');
    }

    set(name, value) {
        if (typeof value !== 'string') {
            value = String(value);
        }
        window[this.type].setItem(this.prefix(name), value);
    }

    get(name) {
        const res = window[this.type].getItem(this.prefix(name));
        if (res) {
            return res;
        }
        return res;
    }

    check(name) {
        const res = this.get(name);
        return res != null;
    }

    remove(name) {
        if (this.check(name)) {
            window[this.type].removeItem(this.prefix(name));
        }
    }

    clear() {
        window[this.type].clear();
    }
}

export default new Storage();
