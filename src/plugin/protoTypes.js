import $storage from '@/libs/storage'; // 本地存储
import $http from '@/api'; // 请求库
import $evn from '@/libs/evn'; // 环境变量

export default {
    $storage,
    $http,
    $evn
};
