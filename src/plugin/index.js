/*
 * @FilePath: \web-course-wx\src\plugin\index.js
 * @Author: wanglipeng
 * @Date: 2021-07-07 15:13:43
 * @LastEditTime: 2021-11-04 17:52:52
 * @LastEditors: wanglipeng
 * @Description: 组件
 */

import prototypes from './protoTypes';

export default {
    install: function(Vue) {
        // 全局变量/方法等
        for (const key in prototypes) {
            Vue.prototype[key] = prototypes[key];
        }
    }
};
