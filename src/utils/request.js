import axios from 'axios';
import storage from '@/libs/storage'; // 本地存储
import evn from '@/libs/evn';
import Router from '../router';
import { MessageBox } from 'element-ui';
const CancelToken = axios.CancelToken;
let cancelOtherRequest = null;
// 配置请求取消
const cancelOption = {
    cancelToken: new CancelToken(function executor(c) {
        cancelOtherRequest = c;
    })
};
class HttpRequest {
    constructor(baseUrl) {
        this.baseUrl = baseUrl;
    }

    /**
     * @description: 错误模板
     * @param {type}
     * @return:
     */
    errorResponse(code = 1, data = null, msg = '接口请求错误') {
        return { code, data, msg };
    }

    /**
     * @description: 配置拦截
     * @param {*} instance
     * @param {*} url
     * @return {*}
     */

    interceptors(instance, url) {
        console.log('url' + Math.random(), url);
        // 请求拦截
        instance.interceptors.request.use((option) => {
            if (option.method === 'post' || option.method === 'put') {
                option.params = {};
            }
            return option;
        });
        // 响应拦截
        instance.interceptors.response.use(
            (res) => {
                const { data, status } = res;
                if (status === 200) {
                    return this.response(data);
                }
                console.log('cancelOtherRequest', cancelOtherRequest);
                return this.errorResponse(status, null, '网络请求错误！');
            },
            (error) => {
                const { msg } = error;
                return this.errorResponse(1, null, msg);
            }
        );
    }

    /**
     * @description: 接口返回结构处理
     * @param {*} res
     * @return {*}
     */
    response(res) {
        const { code } = res;
        if (code === '401' || code === 401) {
            MessageBox.confirm(
                '您的账号登录超时或异常登录，请重新登录！',
                '退出系统',
                {
                    confirmButtonText: '确定',
                    type: 'warning',
                    closeOnClickModal: false,
                    showCancelButton: false,
                    showClose: false,
                    center: true
                }
            ).then(() => {
                window.sessionStorage.clear();
                Router.replace({ name: 'login' });
            });
        }
        return res;
    }

    /**
     * @description: 配置options
     * @param {*}
     * @return {*}
     */
    setOptions() {
        return {
            baseURL: this.baseUrl,
            timeout: 100000,
            responseType: 'json'
        };
    }

    /**
     * @description: 创建实例
     * @param {*} options
     * @return {*}
     */
    init(options) {
        const instance = axios.create();
        options = Object.assign({}, cancelOption, this.setOptions(), options);
        this.interceptors(instance, options.url);
        // 植入token
        const token = storage.get('token') || null;
        if (token) {
            options.headers.access_token = token;
            options.headers.token = token;
        }
        return instance(options);
    }

    /**
     * 请求方式处理：根据实际情况添加请求方式和加密处理
     * @description: 普通的 post请求
     * @param {*} option
     * @return {*}
     */
    post(option) {
        const data = option.params;
        const headers = {};
        const options = Object.assign(
            {},
            { ...option, data, method: 'post', headers }
        );
        return this.init(options);
    }

    /**
     * @description: post请求-json 格式
     * @param {*} post请求
     * @return {*}
     */
    postJson(option) {
        const data = option.params;
        const headers = {
            'Content-Type': 'application/json'
        };
        const options = Object.assign(
            {},
            { ...option, data, method: 'post', headers }
        );
        return this.init(options);
    }

    /**
     * @description: put请求-json 格式
     * @param {*} put请求
     * @return {*}
     */
    put(option) {
        const data = option.params;
        const headers = {
            'Content-Type': 'application/json'
        };
        const options = Object.assign(
            {},
            { ...option, data, method: 'put', headers }
        );
        return this.init(options);
    }

    /**
     * @description: put请求-json 格式
     * @param {*} put请求
     * @return {*}
     */
    delete(option) {
        const data = option.params;
        const headers = {
            'Content-Type': 'application/json'
        };
        const options = Object.assign(
            {},
            { ...option, data, method: 'delete', headers }
        );
        return this.init(options);
    }

    /**
     * @description: post form-data格式 上传
     * @param {*} post请求
     * @return {*}
     */
    postUp(option) {
        const data = option.params;
        const headers = {
            'Content-Type': 'multipart/form-data'
        };
        const options = Object.assign(
            {},
            { ...option, data, method: 'post', headers }
        );
        return this.init(options);
    }

    /**
     * @description: get 请求
     * @param {*} option
     * @return {*}
     */
    get(option) {
        option.method = 'get';
        option.headers = {};
        return this.init(option);
    }
}

export default {
    A: new HttpRequest(evn.baseUrlA),
};
