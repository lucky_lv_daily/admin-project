import Vue from 'vue'
import VueRouter from 'vue-router'
import { MessageBox } from 'element-ui';
import utils from '@/libs/utils';
import storage from '@/libs/storage';
import pageRouter from "./page";

Vue.use(VueRouter)
const routes = [
    {
        path: '/',
        name: 'login',
        meta: {
            title: 'login'
        },
        component: () => import('@/views/public/login/index')
    },
    {
        path: '/page',
        meta: {
            title: '跟目录'
        },
        component: () => import('@/views/pages'),
        children: utils.makePath(pageRouter, '/')
    }
]

const router = new VueRouter({
    routes
})

router.beforeEach((to, from, next) => {
    if (to.name === 'login') {
        next();
    } else {
        if (to.meta.requireAuth) {
            const token = storage.get('token') || false;
            console.log('token', token)
            if (token) {
                next();
            } else {
                MessageBox.confirm(
                    '您的账号登录超时或异常登录，请重新登录！',
                    '退出系统',
                    {
                        confirmButtonText: '确定',
                        type: 'warning',
                        closeOnClickModal: false,
                        showCancelButton: false,
                        showClose: false,
                        center: true
                    }
                ).then(() => {
                    // ok
                    next({
                        name: 'login',
                    });
                });
            }
        } else {
            next();
        }
    }
})

router.afterEach(to => {
    if (to.meta.title) {
        document.title = to.meta.title
    }
})

export default router
