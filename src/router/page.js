const prefix = 'page_'

export default [
    {
        name: prefix + 'home',
        meta: {
            title: '首页'
        },
        component: () => import('@/views/pages/home/index')
    },
    {
        name: prefix + 'one',
        meta: {
            title: 'one',
            requireAuth: true
        },
        component: () => import('@/views/Home')
    },
    {
        name: prefix + 'user',
        meta: {
            title: 'user'
        },
        component: () => import('@/views/UserView')
    }
]
